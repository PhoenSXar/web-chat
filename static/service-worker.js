const dataCacheName = '0.0.1';
const cacheName = 'web-chat-cache';
const filesToCache = [
	'/',
	'/index.html',
	'/style.css',
	'/app.js',
	'/icon-96x96.png',
	'/icon-144x144.png',
	'/icon-144x144.png',
	'/manifest.json'
];

self.addEventListener('install', function (event) {
	console.log('[ServiceWorker] Install');
	event.waitUntil(
		caches.open(cacheName).then(function (cache) {
			console.log('[ServiceWorker] Caching app shell');
			return cache.addAll(filesToCache);
		})
	);
});

self.addEventListener('activate', function (e) {
	console.log('[ServiceWorker] Activate');
	e.waitUntil(
		caches.keys().then(function (keyList) {
			return Promise.all(keyList.map(function (key) {
				if (key !== cacheName && key !== dataCacheName) {
					console.log('[ServiceWorker] Removing old cache', key);
					return caches.delete(key);
				}
			}));
		})
	);
	/*
	 * Fixes a corner case in which the app wasn't returning the latest data.
	 * You can reproduce the corner case by commenting out the line below and
	 * then doing the following steps: 1) load app for first time so that the
	 * initial New York City data is shown 2) press the refresh button on the
	 * app 3) go offline 4) reload the app. You expect to see the newer NYC
	 * data, but you actually see the initial data. This happens because the
	 * service worker is not yet activated. The code below essentially lets
	 * you activate the service worker faster.
	 */
	return self.clients.claim();
});

self.addEventListener('fetch', e => {
	e.respondWith(
		caches.match(e.request).then(cache => {
			return cache || fetch(e.request);
		}).catch(err => {
			console.log(err);
			return fetch(e.requestS);
		})
	);
});
