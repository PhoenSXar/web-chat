'use strict';

const fs = require('fs');
const path = require('path');
const cwd = process.cwd();
const buildDir = path.join(cwd, './build/');
const staticDir = path.join(cwd, './static/');

const fileList = [
  'manifest.json',
  'service-worker.js',
  'icon-96x96.png',
  'icon-144x144.png',
  'icon-240x240.png'
]

console.log(buildDir);
console.log(staticDir);

function copyAll(list) {
  list.forEach(filename => {
    fs.copyFile(path.join(buildDir, filename), path.join(staticDir, filename), (err) => {
      if (err) throw err;
      console.log('copied');
    });
  });
}

copyAll(fileList);



