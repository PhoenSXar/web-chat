const express = require('express');
const bodyParser = require('body-parser');
// const https = require('https');

const http = require('http');

const fs = require('fs');

const { key, cert } = require('../config.json');
const app = express();
app.use(bodyParser.json({ type: '*/json', limit: '32mb' }));
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', express.static('./static'));

app.post('/login', (req, res, next) => {
	res.status(200).send();
});

const options = {
	// key: fs.readFileSync(key),
	// cert: fs.readFileSync(cert),
	hostname: '0.0.0.0'
};

const server = http.createServer(options, app);

const io = require('socket.io')(server);

io.on('connection', function(socket){
	console.log('a user connected');
	socket.on('disconnect', function(){
		console.log('user disconnected');
	});
	socket.on('message', msg => {
		socket.broadcast.emit('message', msg);
		console.log('launch');
	});
});



server.listen(4000);