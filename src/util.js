import store from '@/store';

export const changeHandler = function changeHandler(change) {
	console.log(change);
	const tableName = change.table;
	let changeType;
	switch (change.type) {
	case 1: //CREATED
		changeType = 'CREATED';
		break;
	case 2: //UPDATED
		changeType = 'UPDATED';
		break;
	case 3: //DELETED
		changeType = 'DELETED';
		break;
	}

	const data = {
		id: change.key,
		...change.obj
	}
	
	storeHandler(tableName, changeType, data);
};

export const storeHandler = function storeHandler(tableName, changeType, data) {
	// store.commit('', data);
};