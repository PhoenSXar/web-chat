import Dexie from 'dexie';
import 'dexie-observable';
export const WebChatDB = new Dexie('Web-chat');
import { changeHandler } from '@/util';

WebChatDB.version(1).stores({
	chatList: '++id, name',
	calendarEvent: '++id, detail, startTime',
	note: '++id, detail, updateTime'
});

export const createChatTable = function createChatTable(tableName) {
	WebChatDB.close();
	WebChatDB.version(1).stores({
		[tableName]: '++id, name, type, text'
	});
};

WebChatDB.on('changes', changes => {
	changes.forEach(change => changeHandler(change));
});