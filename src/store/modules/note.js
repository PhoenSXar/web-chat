import { WebChatDB } from '../../model';
import * as types from '../mutations-types';

const state = {
	list: {
		'1': {
			id: 1,
			detail: 'something to do',
			updateTime: Date.now()
		}
	}
};

// noteList = [
// 	{
// 		id: 1,
// 		detail: 'something to do',
// 		updateTime: Date.now()
// 	}
// ]

const mutations = {
	[types.SYNC_NOTE_LIST](state, payload) {
		state.list = payload;
	},
	[types.CREATE_NOTE](state, payload) {
		state.list = { ...state.list, payload }
	},
	[types.UPDATE_NOTE](state, payload) {
		state.list[payload.id] = payload.data;
	},
	[types.DELETE_NOTE](state, payload) {
		state.list[payload.id] = null;
	}
};

export default {
	state,
	mutations
};