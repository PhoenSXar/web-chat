import { WebChatDB } from '../../model';
import * as types from '../mutations-types';

const state = {
	eventList: {
		'1': {
			id: 1,
			detail: 'something to do',
			startTime: Date.now()
		}
	}
};

// event = {
// 	'1': {
// 		id: 1,
// 		detail: 'something to do',
// 		startTime: Date.now()
// 	}
// }

const mutations = {
	[types.SYNC_EVENT_LIST](state, payload) {
		state.eventList = payload.list;
	},
	[types.CREATE_EVENT](state, payload) {
		state.eventList = { ...state, ...payload };
	},
	[types.UPDATE_EVENT](state, payload) {
		state.eventList[payload.id] = payload.data;
	},
	[types.DELETE_EVENT](state, payload) {
		state.eventList[payload.id] = null
	}
};

export default {
	state,
	mutations
};