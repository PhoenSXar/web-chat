import { WebChatDB } from '../../model';
import * as types from '../mutations-types';

const state = {
	list: {
		'1': {
			id: 1,
			name: 'cat1',
			lastTime: new Date().toLocaleString(),
			lastMessage: 'test'
		},
		'2': {
			id: 2,
			name: 'cat1',
			lastTime: new Date().toLocaleString(),
			lastMessage: 'test'
		}
	},
	message: {}
};

// chatId = [
// 	{
// 		id: 1,
// 		name: 'cat1',
// 		lastTime: new Date().toLocaleString(),
// 		lastMessage: 'aoeu'
// 	}
// ];

const mutations = {
	[types.SYNC_CHAT_LIST](state, payload) {
		state.list = payload;
	},
	[types.CREATE_CHAT](state, payload) {
		state.list = { ...state.list, ...payload };
	},
	[types.UPDATE_CHAT](state, payload) {
		state.list[payload.id] = payload.data;
	},
	[types.DELETE_CHAT](state, payload) {
		state.list[payload.id] = null;
	},
	[types.SYNC_MESSAGE](state, payload) {
		state.message = payload;
	},
	[types.PUSH_MESSAGE](state, payload) {
		state.message[payload.id].push(payload.data);
	}
};

// const actions = {
// 	syncChatList({commit}) {
// 		commit([types.SYNC_CHAT_LIST], WebChatDB['chatList'].toArray());
// 	},
// 	syncAllMessage({commit}) {
// 		state.chatId.forEach(chat => {
// 			const logName = chat.id + '-log';
// 			if (!message[logName]) {
// 				commit([types.CREATE_LOG], logName);
// 			}

// 			commit([types.SYNC_LOG], {
// 				logName,
// 				data: WebChatDB[logName].toArray()
// 			});
// 		})
// 	}
// };

export default {
	state,
	mutations
};