export const SYNC_CHAT_LIST = 'SYNC_CHAT_LIST';
export const CREATE_CHAT = 'CREATE_CHAT';
export const UPDATE_CHAT = 'UPDATE_CHAT';
export const DELETE_CHAT = 'DELETE_CHAT';

export const SYNC_MESSAGE = 'SYNC_MESSAGE';
export const PUSH_MESSAGE = 'PUSH_NEW_MESSAGE';

export const SYNC_EVENT_LIST = 'SYNC_EVENT_LIST';
export const CREATE_EVENT = 'CREATE_EVENT';
export const UPDATE_EVENT = 'UPDATE_EVENT';
export const DELETE_EVENT = 'DELETE_EVENT';

export const SYNC_NOTE_LIST = 'SYNC_NOTE_LIST';
export const CREATE_NOTE = 'CREATE_NOTE';
export const UPDATE_NOTE = 'UPDATE_NOTE';
export const DELETE_NOTE = 'DELETE_NOTE'; 