import Vue from 'vue';
import Vuex from 'vuex';
import chat from './modules/chat';
import calendar from './modules/calendar';
import note from './modules/note';

Vue.use(Vuex);
Vue.config.devtools = true;

const store = new Vuex.Store({
	strict: true,
	modules: {
		chat,
		calendar,
		note
	}
});

export default store;