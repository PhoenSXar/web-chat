'use strict';
import Account from './pages/account/account.vue';
import Register from './pages/account/register.vue';
import Binding from './pages/account/binding.vue';
import Info from './pages/account/info.vue';

import Index from './pages/index.vue';
import Message from './pages/message/message.vue';

import MessageContent from './pages/message/content.vue';

import Circle from './pages/find/circle.vue';
import See from './pages/find/see.vue';

import PersonalInfo from './pages/personal/personal-info.vue';
import Collection from './pages/personal/collection.vue';

import Follow from './pages/personal/follow/follow.vue';

import Track from './pages/personal/track.vue';

import Activity from './pages/personal/activity/activity.vue';
import ActivityDetail from './pages/personal/activity/activity-detail.vue';

import Advice from './pages/personal/advice/advice.vue';
import AdviceDetail from './pages/personal/advice/advice-detail.vue';

import Feedback from './pages/personal/feedback.vue';

import Setting from './pages/personal/setting/setting.vue';
import MessageRemind from './pages/personal/setting/message-remind.vue';
import ModifyPassword from './pages/personal/setting/modify-password.vue';
import About from './pages/personal/setting/about.vue';
import Introduction from './pages/personal/setting/introduction.vue';

import VoteIndex from './pages/vote/vote-index.vue';
import Vote from './pages/vote/vote.vue';
import Questionnaire from './pages/vote/questionnaire.vue';

import Knowledge from './pages/home/knowledge.vue';

import Schedule from './pages/schedule/schedule.vue';
import ScheduleDetail from './pages/schedule/detail.vue';

import Note from './pages/note/note.vue';
import NoteDetail from './pages/note/detail.vue';

import Join from './pages/message/join.vue';
import Login from './pages/account/login.vue';

export default [
	{
		path: '/login',
		component: Login
	},
	{
		path: '/',
		component: Account
	},
	{
		path: '/register',
		component: Register
	},
	{
		path: '/binding',
		component: Binding
	},
	{
		path: '/info',
		component: Info
	},

	{
		path: '/index/',
		component: Index,
		tabs: [
			{
				path: '/',
				id: 'message',
				component: Message
			},
			{
				path: '/schedule/',
				id: 'schedule',
				component: Schedule
			},
			{
				path: '/note/',
				id: 'note',
				component: Note
			}
		]
	},
	{
		path: '/addmessage',
		component: Join
	},
	{
		path: '/addschedule',
		component: ScheduleDetail
	},
	{
		path: '/addnote',
		component: NoteDetail
	},
	{
		path: '/message-content/:id',
		component: MessageContent
	},
	{
		path: '/schedule-detail/:id',
		component: ScheduleDetail
	},
	{
		path: '/note-detail/:id',
		component: NoteDetail
	},
	{
		path: '/circle',
		component: Circle
	},
	{
		path: '/see',
		component: See
	},

	{
		path: '/personal-info',
		component: PersonalInfo
	},
	{
		path: '/collection',
		component: Collection
	},
	{
		path: '/follow',
		component: Follow
	},
	{
		path: '/track',
		component: Track
	},
	{
		path: '/activity',
		component: Activity
	},
	{
		path: '/activity-detail',
		component: ActivityDetail
	},

	{
		path: '/advice',
		component: Advice
	},
	{
		path: '/advice-detail',
		component: AdviceDetail
	},

	{
		path: '/feedback',
		component: Feedback
	},

	{
		path: '/setting',
		component: Setting
	},
	{
		path: '/message-remind',
		component: MessageRemind
	},
	{
		path: '/modify-password',
		component: ModifyPassword
	},
	{
		path: '/about',
		component: About
	},
	{
		path: '/introduction',
		component: Introduction
	},

	{
		path: '/vote-index',
		component: VoteIndex
	},
	{
		path: '/vote',
		component: Vote
	},
	{
		path: '/questionnaire',
		component: Questionnaire
	},
	{
		path: '/knowledge',
		component: Knowledge
	},
];