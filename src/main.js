'use strict';

import Vue from 'vue';
import Framework7 from 'framework7';
import Framework7Vue from 'framework7-vue';

import vueEventCalendar from 'vue-event-calendar';

Vue.use(Framework7Vue, Framework7);
Vue.use(vueEventCalendar, {locale: 'en'});
Vue.config.devtools = true;

import { WebChatDB } from './model.js';

window.db = WebChatDB;

import './css/app.less';
import 'vue-event-calendar/dist/style.css';

import routes from './routes.js';
import store from './store';
import App from './app.vue';
const $app = new Vue(Object.assign({
	store,
	template: '<app/>',
	framework7: {
		id: 'web-chat',
		name: 'web-chat',
		theme: 'auto',
		material: true,
		routes: routes
	}
}, App));

$app.$mount('#app');

store.commit('SYNC_CHAT_LIST', {});
store.commit('SYNC_MESSAGE', {});

if ('serviceWorker' in navigator) {
	window.addEventListener('load', function () {
		navigator.serviceWorker.register('./service-worker.js', {scope: '/'})
			.then(function (registration) {

				// 注册成功
				console.log('ServiceWorker registration successful with scope: ', registration.scope);
			})
			.catch(function (err) {

				// 注册失败:(
				console.log('ServiceWorker registration failed: ', err);
			});
	});
}